import socket

s = socket.socket()
print("Socket Created")
s.bind(('localhost',9999))
s.listen(3)
print("Waiting for Connections")

while True:
    c , address = s.accept()
    name = c.recv(1024).decode("utf-8")
    print("Client is Connected : ",name)
    c.send(bytes("Welcome to SULTANS Land","utf-8"))
    c.close()
    